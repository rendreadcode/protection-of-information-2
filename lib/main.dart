// Flutter code sample for AppBar

// This sample shows an [AppBar] with two simple actions. The first action
// opens a [SnackBar], while the second action navigates to a new page.

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart';


void main() => runApp(MyApp());

/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = 'First Laba';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      home: MyStatefulWidget(),
    );
  }
}

final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
final SnackBar snackBar = const SnackBar(content: Text('made by Dima & Julia'));

void openPage(BuildContext context) {
  Navigator.push(context, MaterialPageRoute(
    builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Next page'),
        ),
        body: const Center(
          child: Text(
            'Следующая страница',
            style: TextStyle(fontSize: 24),
          ),
        ),
      );
    },
  ));
}
TextEditingController counterController = new TextEditingController();
TextEditingController outputController = new TextEditingController();


class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}
int count = 0;
/// This is the stateless widget that the main application instantiates.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      
      appBar: AppBar(
        backgroundColor: Colors.blue[100],
        title: const Text('Первая лаба'),
        actions: <Widget>[
          IconButton(
            color: Colors.redAccent,
            icon: const Icon(Icons.add_alert),
            tooltip: 'made by Dima & Julia',
            onPressed: () {
              scaffoldKey.currentState.showSnackBar(snackBar);
            },
          ),
          IconButton(
            color: Colors.redAccent,
            icon: const Icon(Icons.navigate_next),
            tooltip: 'некст страница',
            onPressed: () {
              openPage(context);
            },
          ),
        ],
      ),
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Colors.blue,  Colors.white]
            )
          ),
          child: Container(
            padding: const EdgeInsets.all(40.0),
            alignment: Alignment.topCenter,
            child: Column(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.fromLTRB(0, 20.0, 0, 20.0),
                  child: 
                    TextField(
                      controller: counterController,
                      decoration: new InputDecoration(labelText: "Введите число",),
                      keyboardType: TextInputType.number,
                      maxLength: 15,
                      maxLines: 1,
                    ),
                ),
                Container(
                    padding: const EdgeInsets.fromLTRB(0, 20.0, 0, 20.0),
                    child: 
                      TextField(
                        readOnly: true,
                        controller: outputController,
                        decoration: new InputDecoration(labelText: "Число после перестановки:",),
                        maxLength: 15,
                        maxLines: 1,
                  ),
                ),
              ],              
            ),
          ),
          //TextField(
          //    decoration: new InputDecoration(labelText: "Введите число",),
          //    keyboardType: TextInputType.number,
        ),
        //child: Text(
        //  '123',
        //  style: TextStyle(fontSize: 24),
        //),
      ),
      bottomNavigationBar: BottomAppBar(
      
      color: Colors.blueAccent[100],
      shape: const CircularNotchedRectangle(),
      child: Container(height: 50.0,),
    ),
    floatingActionButton: FloatingActionButton(
        
        onPressed: () => setState(() {
          sort();

          outputController.text = count.toString();
        }),
        tooltip: 'Пересчитать',
        child: Icon(Icons.restore),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
    
  }
}

void sort(){

  List<int> math = new List<int>();
  List<String> mathStr;

  if (counterController.text=='')
  {
    getStr().then((stringFile){
      counterController.text=stringFile.toString();
    });
  }
  mathStr = counterController.text.split('');

  for (int i=0; i<counterController.text.length;i++)
  {
    int pere = int.parse(mathStr[i]);
    math.add(pere);
  }
  int temp;

  for (int i=0; i < counterController.text.length;i++)
  {

    for (int j=0; j < counterController.text.length-i-1;j++)
    {

      if (math[j+1] > math[j])
      {
        temp = math[j+1];
        math[j+1] = math[j];
        math[j] = temp;
      }
    }
  }
  String str="";
    for (int i=0;i<counterController.text.length;i++){
      str= str + math[i].toString(); 
    }
    count = int.parse(str);
    writeCounter(count);
    getLastCounter();
}
  

Future<String> getStr() async {
  var strqwe = rootBundle.loadString('assets/input.txt') ;
  final response = await strqwe;
  return response.toString();
}


void getLastCounter() async {
  Directory appDocDir = await getApplicationDocumentsDirectory();
  

  String filePath = '${appDocDir.path}/file.txt';
  new File(filePath).readAsString().then((String contents) {
  print(contents);
}); 
}

Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();

  return directory.path;
}

Future<File> get _localFile async {
  final path = await _localPath;
  return File('$path/file.txt');
}

Future<File> writeCounter(int counter) async {
  final file = await _localFile;

  // Write the file.
  return file.writeAsString('$counter');
}